# Effect

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**balance_changes** | Option<[**::std::collections::HashMap<String, crate::models::BalanceChange>**](balance_change.md)> | Balance changes by asset | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


