# PlatformDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**source** | Option<**String**> | Backend API Type | [optional]
**handle** | Option<**String**> |  | [optional]
**genesis_number** | Option<**i64**> |  | [optional]
**endpoints** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


