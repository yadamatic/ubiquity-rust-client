# Report

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**fields** | [**Vec<crate::models::ReportField>**](report_field.md) | Transaction items | 
**items** | **i32** | The number of transactions in the report | 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


